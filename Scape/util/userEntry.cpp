//
//  userEntry.cpp
//  Scape
//
//  Created by Joseph Livengood on 9/9/17.
//  Copyright © 2017 Joseph Livengood. All rights reserved.
//

#include "userEntry.hpp"

#include <iostream>

// optional overrideText
// overload methods instead?
int getIntValueFromUser(std::string overrideText) {
    const std::string defaultText = "Enter an integer: "; // non-prim cant have constexpr
    if (overrideText.empty())
        std::cout << defaultText;
    else
        std::cout << overrideText;
    int a;
    std:: cin >> a;
    return a;
}
