//
//  userEntry.hpp
//  Scape
//
//  Created by Joseph Livengood on 9/9/17.
//  Copyright © 2017 Joseph Livengood. All rights reserved.
//

#ifndef userEntry_hpp
#define userEntry_hpp

#include <stdio.h>
#include <string>

int getIntValueFromUser(std::string overrideText = "");

#endif /* userEntry_hpp */
