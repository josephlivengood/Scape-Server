//
//  main.cpp
//  Scape
//
//  Created by Joseph Livengood on 9/9/17.
//  Copyright © 2017 Joseph Livengood. All rights reserved.
//

#include <iostream>

#include "util/userEntry.hpp"
#include "constants.hpp"

// forward declaration
void doHello();
void doNumberEntry();
void givePie();

int main(int argc, const char * argv[]) {
    doHello();
    doNumberEntry();
    givePie();
    return 0;
}

void doHello() {
    std::string x;
    x = "Hello.";
    std::cout << x << std::endl; // << "\n";
    std::cin >> x;
    x = x + "\n";
    std::cout << x;
}

int multiply(const int num1, const int num2 = 6) {
    return num1 * num2;
}

void doNumberEntry() {
    int a = getIntValueFromUser();
    std::cout << "You entered " << a << std::endl;
    std::cout << a << " times 6 equals " << multiply(a, 6) << "." << std::endl;
}

void givePie() {
    std::cout << constants::pi << std::endl;
}
