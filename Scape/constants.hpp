//
//  constants.hpp
//  Scape
//
//  Created by Joseph Livengood on 9/9/17.
//  Copyright © 2017 Joseph Livengood. All rights reserved.
//

#ifndef constants_hpp
#define constants_hpp

namespace constants {
    const double pi(3.14159);
    const double avogadro(6.0221413e23);
}

#endif /* constants_hpp */
